package com.minseok.monthlyearningcalculator.base

/**
 * Created by minseok on 2018. 6. 12..
 * MonthlyEarningCalculator.
 */
interface BasePresenter {
    fun onStart()
    fun onStop()
}