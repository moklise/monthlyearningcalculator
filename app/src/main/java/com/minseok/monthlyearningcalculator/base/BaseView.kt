package com.minseok.monthlyearningcalculator.base

/**
 * Created by minseok on 2018. 6. 12..
 * MonthlyEarningCalculator.
 */
interface BaseView<in T : BasePresenter> {
    fun setPresenter(presenter: T)
}