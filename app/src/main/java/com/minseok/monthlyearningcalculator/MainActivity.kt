package com.minseok.monthlyearningcalculator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), MainContract.View {
    lateinit var mPresenter: MainContract.Presenter
    val curDate = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainPresenter(this)

        setApplicationConfiguration()

        setCalendar()
        setEditListener()
        setDayEditListener()

        initDate()
    }

    private fun initDate() {
        val today = Calendar.getInstance()
        mPresenter.loadPayInfo(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH))

        refershTotalData()
    }

    private fun setCalendar() {
        layout_calander.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val targetTime = Calendar.getInstance().apply {
                this.set(Calendar.YEAR, year)
                this.set(Calendar.MONTH, month)
                this.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            }

            /* Day Setting */
            txt_day_date.text = "${month + 1}월 ${dayOfMonth}일"

            /* Total */
            txt_total_month.text = "${month + 1}월"

            val dayTime = mPresenter.getTotalTime(targetTime.timeInMillis)
            txt_total_time.text = "${dayTime.hour}시간 ${dayTime.min}분"
            txt_total_salary.text = String.format("%,d원", mPresenter.getTotalSalary(targetTime.timeInMillis))

            curDate.apply {
                this.set(Calendar.YEAR, year)
                this.set(Calendar.MONTH, month)
                this.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            }

            mPresenter.loadPayInfo(year, month, dayOfMonth)
            this.onSuccessSavePayInfo()
        }
    }

    private fun refershTotalData() {
        val dayTime = mPresenter.getTotalTime(curDate.timeInMillis)
        txt_total_time.text = "${dayTime.hour}시간 ${dayTime.min}분"
        txt_total_salary.text = String.format("%,d원", mPresenter.getTotalSalary(curDate.timeInMillis))
    }

    private fun setEditListener() {
        edit_day_hour.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable) {
                if (!p0.isEmpty() && p0.toString().toInt() >= 24) {
                    edit_day_hour.setText("23")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        edit_day_hour.setOnFocusChangeListener { _, b -> if (b) edit_day_hour.setText("") }
        edit_day_min.setOnFocusChangeListener { _, b -> if (b) edit_day_min.setText("") }

        edit_day_min.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable) {
                if (!p0.isEmpty() && p0.toString().toInt() >= 60) {
                    edit_day_min.setText("59")
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    private fun setDayEditListener() {
        btn_save_or_edit.setOnClickListener {
            val button = it as Button
            when (button.text) {
                "EDIT" -> {
                    if (edit_day_hour.text.toString() == "0" && edit_day_min.text.toString() == "0"
                    && edit_end_day_hour.text.toString() == "0" && edit_end_day_hour.text.toString() == "0") {
                        edit_day_hour.setText("")
                        edit_day_min.setText("")
//                        edit_day_worktime.setText("")

                        edit_end_day_hour.setText("")
                        edit_end_day_min.setText("")
                    }

                    this.onStartEditPayInfo()
                }
                "SAVE" -> {
                    val startHour = edit_day_hour.text.toString().let { if (it.isEmpty()) 0 else it.toInt() }
                    val startMin  = edit_day_min .text.toString().let { if (it.isEmpty()) 0 else it.toInt() }
                    val endHour   = edit_end_day_hour.text.toString().let { if (it.isEmpty()) 0 else it.toInt() }
                    val endMin    = edit_end_day_min .text.toString().let { if (it.isEmpty()) 0 else it.toInt() }

                    mPresenter.savePayInfo(curDate.timeInMillis, DayTime(startHour, startMin), DayTime(endHour, endMin))
                    refershTotalData()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.item_menu, menu);
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.edit_payhour -> {
                Toast.makeText(this, "수정", Toast.LENGTH_SHORT).show()
                mPresenter.setPayhour(0)
                refershTotalData()
                true
            }
             else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setDayTime(time: DayTime) {
        txt_day_total.text = "${time.hour}시간 ${time.min}분"
    }

    override fun onSuccessSavePayInfo() {
        edit_day_hour.isEnabled = false
        edit_day_min.isEnabled = false

        edit_end_day_hour.isEnabled = false
        edit_end_day_min.isEnabled = false

        btn_save_or_edit.text = "EDIT"
    }

    override fun onStartEditPayInfo() {
        edit_day_hour.isEnabled = true
        edit_day_min.isEnabled = true

        edit_end_day_hour.isEnabled = true
        edit_end_day_min.isEnabled = true

        btn_save_or_edit.text = "SAVE"
    }

    override fun showTargetDatePayInfo(startTime: DayTime, endTime: DayTime) {
        edit_day_hour.setText(startTime.hour.toString())
        edit_day_min.setText(startTime.min.toString())

        edit_end_day_hour.setText(endTime.hour.toString())
        edit_end_day_min.setText(endTime.min.toString())
    }

    override fun toast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    private fun setApplicationConfiguration() {
        Realm.init(this)
    }

    override fun setPresenter(presenter: MainContract.Presenter) {
        mPresenter = presenter
    }
}
