package com.minseok.monthlyearningcalculator

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by minseok on 2018. 6. 14..
 * MonthlyEarningCalculator.
 */
open class PayRecord() : RealmObject() {
    @PrimaryKey
    var index: Int = 0
    var year: Int = 0
    var month: Int = 0
    var day: Int = 0
    var startHour: Int = 0
    var startMin: Int = 0
    var endHour: Int = 0
    var endMin: Int = 0
    var workTime: Int = 0
    var payPerHour: Int = 8000

    constructor(index: Int, year: Int, month: Int, day: Int) : this() {
        this.index = index
        this.year = year
        this.month = month
        this.day = day
        this.startHour = 0
        this.startMin = 0
        this.endHour = 0
        this.endMin = 0
        this.workTime = 0
        this.payPerHour = 0
    }

    constructor(index: Int, year: Int, month: Int, day: Int, startHour: Int, startMin: Int, endHour: Int, endMin: Int, payPerHour: Int) : this() {
        this.index = index
        this.year = year
        this.month = month
        this.day = day
        this.startHour = startHour
        this.startMin = startMin
        this.endHour = endHour
        this.endMin = endMin
        this.workTime = workTime
        this.payPerHour = payPerHour
    }
}