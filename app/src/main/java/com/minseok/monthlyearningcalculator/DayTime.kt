package com.minseok.monthlyearningcalculator

import java.util.*

/**
 * Created by minseok on 2018. 6. 21..
 * MonthlyEarningCalculator.
 */
class DayTime {
    private var pair: Pair<Int, Int>

    constructor(startHour: Int, startMin: Int, endHour :Int, endMin: Int) {
        val startDate = Calendar.getInstance().apply {
            this.set(Calendar.HOUR, startHour)
            this.set(Calendar.MINUTE, startMin)
        }

        val endDate = Calendar.getInstance().apply {
            this.set(Calendar.HOUR, endHour)
            this.set(Calendar.MINUTE, endMin)
        }

        val differenceMillis = endDate.timeInMillis - startDate.timeInMillis
        val tempMilisSec = differenceMillis / 1000
        val offsetMin = (tempMilisSec % 3600 / 60).toInt()
        val offsetHour = (tempMilisSec / 3600).toInt()

        pair = Pair(offsetHour, offsetMin)
    }

    constructor(startTime: DayTime, endTime: DayTime) {
        val tempDayTime = DayTime(startTime.hour, startTime.min, endTime.hour, endTime.min)

        pair = Pair(tempDayTime.hour, tempDayTime.min)
    }

    constructor(hour: Int, min: Int) {
        pair = Pair( hour, min)
    }

    val hour: Int
        get() = pair.first

    val min: Int
        get() = pair.second

    operator fun plus(dayTime: DayTime): DayTime {
        var tempHour = this.hour + dayTime.hour
        var tempMin = this.min + dayTime.min

        if (tempMin >= 60) {
            tempHour += 1
            tempMin -= 60
        }

        return DayTime(tempHour,tempMin)
    }
}