package com.minseok.monthlyearningcalculator

import android.util.Log
import io.realm.Realm
import java.util.*

/**
 * Created by minseok on 2018. 6. 12..
 * MonthlyEarningCalculator.
 */
class MainPresenter(val mView: MainContract.View) : MainContract.Presenter {
    var payPerHour: Int = 8000

    override fun onStart() {}

    override fun onStop() {}

    override fun getTotalSalary(date: Long): Int {
        val totalTime = getTotalTime(date)

        val totalTimeF = totalTime.hour.toFloat() + (totalTime.min.toFloat() / 60F)

        val pay = (payPerHour * totalTimeF).toInt()

        return pay - pay % payPerHour
    }

    override fun getTotalTime(date: Long): DayTime {
        val curDate = Calendar.getInstance().apply { this.timeInMillis = date }
        val year = curDate.get(Calendar.YEAR)
        val month = curDate.get(Calendar.MONTH)

        val list = Realm.getDefaultInstance().where(PayRecord::class.java)
                .equalTo("year", year)
                .equalTo("month", month)
                .findAll()

        var total = DayTime(0,0)
        list.forEach {
            total += DayTime(it.startHour, it.startMin, it.endHour, it.endMin)
        }

        return total;
    }


    override fun loadPayInfo(year: Int, month: Int, dayOfMonth: Int) {
        val mRealm = Realm.getDefaultInstance()
        val targetDate = mRealm.where(PayRecord::class.java)
                .equalTo("year", year)
                .equalTo("month", month)
                .equalTo("day", dayOfMonth)
                .findFirst()


        targetDate?.let {
            mView.showTargetDatePayInfo(DayTime(targetDate.startHour, targetDate.startMin), DayTime(targetDate.endHour, targetDate.endMin))
            mView.setDayTime(DayTime(targetDate.startHour, targetDate.startMin, targetDate.endHour, targetDate.endMin))
        } ?: let{
            mView.showTargetDatePayInfo(DayTime(0, 0), DayTime(0, 0))
            mView.setDayTime(DayTime(0,0,0,0))
        }
    }

    override fun savePayInfo(date: Long, startTime: DayTime, endTime: DayTime) {
        val curDate = Calendar.getInstance().apply {timeInMillis = date}
//        val curDate = GregorianCalendar().apply { this.time = Date(date)}

        Log.d("EarningByTimeLog",curDate.toString())

        val curYear = curDate.get(Calendar.YEAR)
        val curMonth = curDate.get(Calendar.MONTH)
        val curDay = curDate.get(Calendar.DAY_OF_MONTH)

        val mRealm = Realm.getDefaultInstance()
        val targetDate = mRealm.where(PayRecord::class.java)
                .equalTo("year", curYear)
                .equalTo("month", curMonth)
                .equalTo("day", curDay)
                .findFirst() ?: let {
            PayRecord(getPayRecordCount(), curYear, curMonth, curDay);
        }

        mRealm.beginTransaction()
        targetDate.startHour = startTime.hour
        targetDate.startMin = startTime.min
        targetDate.endHour = endTime.hour
        targetDate.endMin = endTime.min
        mRealm.insertOrUpdate(targetDate)
        mRealm.commitTransaction()

        mView.onSuccessSavePayInfo()
        mView.setDayTime(DayTime(startTime, endTime))
    }

    override fun setPayhour(payhour: Int) {
        payPerHour = payhour;
    }

    private fun getPayRecordCount(): Int = Realm.getDefaultInstance().where(PayRecord::class.java).count().toInt()


    init {
        mView.setPresenter(this)
    }
}