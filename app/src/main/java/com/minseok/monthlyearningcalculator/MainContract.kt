package com.minseok.monthlyearningcalculator

import com.minseok.monthlyearningcalculator.base.BasePresenter
import com.minseok.monthlyearningcalculator.base.BaseView

/**
 * Created by minseok on 2018. 6. 12..
 * MonthlyEarningCalculator.
 */
interface MainContract {
    interface View : BaseView<Presenter> {
        fun showTargetDatePayInfo(startTime: DayTime, endTime: DayTime)

        fun onStartEditPayInfo()

        fun onSuccessSavePayInfo()

        fun setDayTime(time: DayTime)

        fun toast(msg: String)
    }

    interface Presenter : BasePresenter {
        fun getTotalSalary(date: Long): Int

        fun getTotalTime(date: Long): DayTime

        fun loadPayInfo(year: Int, month: Int, dayOfMonth: Int)

        fun savePayInfo(date: Long, startTime: DayTime, endTime: DayTime)

        fun setPayhour(payhour: Int)
    }
}